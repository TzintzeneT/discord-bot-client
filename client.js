const readline = require('readline');

const Input = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const TOKEN = 'YOUR_TOKEN_HERE'

const {Client, GatewayIntentBits, ChannelTypes, TextChannel} = require('discord.js');
const client = new Client({
    intents: [
        GatewayIntentBits.Guilds
    ]
})

//when the bot is connected
client.on('ready', () => {
    console.log(`${client.user.tag} is saying hello world!`);
    sendAMessage()
});

//TUI colors
const RED = '\u001b[1;31m'
const BLUE = '\u001b[1;34m'
const PURPLE = '\u001b[1;35m'
const YELLO = '\u001b[1;33m'
const CYAN = '\u001b[1;36m'
const GRAY = '\u001b[1;37m'
const GREEN = '\u001b[1;32m'

function sendAMessage() {
    console.log()
    printServers()
    console.log()

    Input.question(BLUE + 'Server: \u001b[0m', (guildNum) => {
        console.log()
        console.log(CYAN + ' All text channels:\u001b[0m')
        console.log()
        const selectedServer = client.guilds.cache.at(guildNum)

        try {
            for (let i = 0; i < selectedServer.channels.cache.size; i++) {
                if (selectedServer.channels.cache.at(i).type === 0) {
                    console.log('  ' + i + ') ' + selectedServer.channels.cache.at(i).name)
                }
            }
            console.log()

            Input.question(BLUE + 'Channel: \u001b[0m', (channelNum) => {
                const selectedChannel = selectedServer.channels.cache.at(channelNum)
                console.log()

                Input.question(BLUE + 'Message: \u001b[0m', (message) => {
                    if (message.length > 0) {
                        try {
                            selectedChannel.send(message)
                        } catch (error) {
                            console.log()
                            console.log(RED + 'This is NOT an option\u001b[0m')
                        }
                    } else {
                        console.log()
                        console.log(RED + 'Cannot send an empty message\u001b[0m')
                    }
                    sendAMessage()
                })
            })
        } catch (error) {
            console.log(RED + 'This is NOT an option\u001b[0m')
            sendAMessage()
        }
    })
}

function printServers() {
    console.log(CYAN + ' All available servers:\u001b[0m')
    console.log()
    for (let i = 0, guild = client.guilds.cache.at(0); i < client.guilds.cache.size; i++, guild = client.guilds.cache.at(i)) {
        console.log('  ' + i + ') ' + guild.name)
    }
}

// function printAllOptions(){
//     for (let i = 0, guild = client.guilds.cache.at(0) ; i < client.guilds.cache.size; i++, guild = client.guilds.cache.at(i)){
//         console.log(i + ') ' + guild.name)
//         for (let j = 0, channel = guild.channels.cache.at(0); j < guild.channels.cache.size; j++, channel = guild.channels.cache.at(j)){
//             console.log(' -' + j + ') ' + channel.name)
//         }
//         console.log()
//     }
// }

//login bot using token
client.login(TOKEN);
